package eartlandRPG.main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Menu {
	
	public String[] options = {"new game", "load game", "exit"};
	
	public int currentOption = 0;
	public int maxOption = options.length - 1;
	
	public boolean up, down, enter;
	public boolean pause = false;
	
	public void tick() {
		if(up) {
			Sound.menuSelect.play();
			up = false;
			this.currentOption--;
			if(this.currentOption < 0)
				this.currentOption = this.maxOption;
		}
		if(down) {
			Sound.menuSelect.play();
			down = false;
			this.currentOption++;
			if(this.currentOption > this.maxOption)
				this.currentOption = 0;
		}
		if(enter) {
			Sound.menuEnter.play();
			Sound.backgroundSound.play();
			enter = false;
			if(options[this.currentOption] == "new game" || options[this.currentOption] == "resume game") {
				Game.gameState = "NORMAL";
				pause = false;
			}else if (options[currentOption] == "exit") {
				System.exit(1);
			}
		}
	}
	
	public void render(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, Game.WIDTH*Game.SCALE, Game.HEIGHT*Game.SCALE);
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.BOLD, 70));
		g.drawString("Tales of a Dead Man", (Game.WIDTH*Game.SCALE)/2 - 330, (Game.HEIGHT*Game.SCALE)/2 - 220);
		
		//Menu options
		
		g.setFont(new Font("arial", Font.BOLD, 36));
		if(pause == false)
			g.drawString("New game", (Game.WIDTH*Game.SCALE)/2 + 220, (Game.HEIGHT*Game.SCALE)/2 + 200);
		else {
			Sound.backgroundSound.stop();
			g.drawString("Resume game", (Game.WIDTH*Game.SCALE)/2 + 220, (Game.HEIGHT*Game.SCALE)/2 + 200);
		}
		g.drawString("Load game", (Game.WIDTH*Game.SCALE)/2 + 220, (Game.HEIGHT*Game.SCALE)/2 + 240);
		g.drawString("Exit", (Game.WIDTH*Game.SCALE)/2 + 220, (Game.HEIGHT*Game.SCALE)/2 + 280);
		
		if(options[currentOption] == "new game") {
			g.drawString("> ", (Game.WIDTH*Game.SCALE)/2 + 190, (Game.HEIGHT*Game.SCALE)/2 + 200);
		}else if(options[currentOption] == "load game") {
			g.drawString("> ", (Game.WIDTH*Game.SCALE)/2 + 190, (Game.HEIGHT*Game.SCALE)/2 + 240);
		}else if(options[currentOption] == "exit") {
			g.drawString("> ", (Game.WIDTH*Game.SCALE)/2 + 190, (Game.HEIGHT*Game.SCALE)/2 + 280);
		}
	}

}
