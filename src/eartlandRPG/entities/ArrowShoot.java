package eartlandRPG.entities;

//import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import eartlandRPG.main.Game;
import eartlandRPG.world.Camera;
//import eartlandRPG.world.World;

public class ArrowShoot extends Entity{
	
	private double dx;
	private double dy;
	private double spd = 4;
	private BufferedImage[] arrowUp;
	private BufferedImage[] arrowDown;
	private BufferedImage[] arrowRight;
	private BufferedImage[] arrowLeft;
	private int arrowDir;
	public int right_dir = 0, left_dir = 1, up_dir = 2, down_dir = 3;

	private int maxRange = 50, range = 0;
	
	public ArrowShoot(int x, int y, int width, int height, BufferedImage sprite, double dx, double dy, int dir) {
		super(x, y, width, height, sprite);
		arrowUp = new BufferedImage[2];
		arrowDown = new BufferedImage[2];
		arrowRight =  new BufferedImage[2];
		arrowLeft = new BufferedImage[2];
		arrowDir = dir;
		for(int i = 0; i < 2; i++) {
			arrowUp[i] = Game.spritesheet.getSprite(0*16 + (i*16), 3*16, 16, 16);
		 }
		 for(int i = 0; i < 2; i++) {
			 arrowDown[i] = Game.spritesheet.getSprite(0*16 + (i*16), 4*16, 16, 16);
		 }
		 for(int i = 0; i < 2; i++) {
			 arrowRight[i] = Game.spritesheet.getSprite(0*16 + (i*16), 6*16, 16, 16);
		 }
		 for(int i = 0; i < 2; i++) {
			 arrowLeft[i] = Game.spritesheet.getSprite(0*16 + (i*16), 5*16, 16, 16);
		 }
		this.dx = dx;
		this.dy = dy;
	}
	
	public void tick() {
		x+=dx*spd;
		y+=dy*spd;
		range++;
		if(range == maxRange) {
			Game.arrows.remove(this);
			return;
		}
	}
	
	public void render(Graphics g) {
		if(arrowDir == up_dir) {
			g.drawImage(Entity.ARROW_UP, this.getX()+4 - Camera.x, this.getY()+2 - Camera.y, null);
		}
		if(arrowDir == down_dir) {
			g.drawImage(Entity.ARROW_DOWN, this.getX()+4 - Camera.x, this.getY()+2 - Camera.y, null);
		}
		if(arrowDir == left_dir) {
			g.drawImage(Entity.ARROW_LEFT, this.getX()+4 - Camera.x, this.getY()+2 - Camera.y, null);
		}
		if(arrowDir == right_dir) {
			g.drawImage(Entity.ARROW_RIGHT, this.getX()+4 - Camera.x, this.getY()+2 - Camera.y, null);
		}
		
//		g.setColor(Color.YELLOW);
//		g.fillOval(this.getX() - Camera.x, this.getY() - Camera.y, 2, 2);
	}
	
}
